
const config = require("./config/config.json")
const JsonDB = require("node-json-db").JsonDB
const Config = require("node-json-db/dist/lib/JsonDBConfig").Config

const db = new JsonDB(new Config(__dirname + "/db", true, true, '/'))

//////////////////////
// Admin
//////////////////////

const isAdmin = userId => {
    return userId == config.adminUser
}

//////////////////////
// Roles
//////////////////////

const getRoles = () => {
    try {
        return db.getData("/roles")
    } catch (error) {
        return []
    };
}

const hasRole = roleName => {
    return getRoles().find(name => name == roleName) != undefined
}

const addRole = roleName => {
    db.push("/roles[]", roleName, false)
}

//////////////////////
// Export
//////////////////////

module.exports = {
    isAdmin,

    getRoles,
    hasRole,
    addRole,
}

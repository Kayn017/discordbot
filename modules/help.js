
const Discord = require("discord.js")

exports.name = "help"

exports.commands = [
    "/help : Affiche l'aide"
]

exports.onMessage = async (msg, discordClient, modules) => {

    if (msg.content.trim() == "/help")
    {
        const commands = modules.map(module => module.commands || []).reduce((acc, val) => [...acc, ...val])

        msg.channel.send(new Discord.MessageEmbed()
            .setTitle("Liste des commandes")
            .setDescription(commands.join("\n")))

        return true
    }

}

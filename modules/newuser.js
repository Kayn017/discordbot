const { serverID, adminUser } = require("../config/config.json");

/* Etapes inscription */
const etapes = require("../config/etapes_mp.json");

let newUsers = {};

exports.name = "newuser";

exports.commands = [
    "/resetGroups : reset les groupes pour les années/semestres et les redemandes aux utilisateurs (administrateur)"
];


exports.onMessage = async (msg) => 
{
	//si l'admin décide reset les groupes
	if(msg.content == '/resetGroups' && msg.author.id == adminUser)
		return resetGroups(msg.client);

	//si le message ne provient pas d'un utilisateur dont on souhaite ou si c'est un message du style MACHIN VIENT DE REJOINDRE LE SERVEUR
	if(!newUsers[msg.author.id] || msg.type == 'GUILD_MEMBER_JOIN')
		return false;


	receptionMessageNewUser(msg);
	
	return true;
	
}

exports.newUser = async (newUser) =>
{
	newUsers[newUser.id] = {
		step : 'eleve_annee',
		semestre : null,
		groupe : null,
		name : null		
	};

	await newUser.send("Salut ! Bienvenue sur le serveur de l'IUT ! ");

	return envoiMessageInscription(newUser);

}



function envoiMessageInscription(nuser)
{
	//on voit a quel étape de l'inscription est l'utilisateur
	let etape = etapes[newUsers[nuser.id].step];

	//on lui envoie le message correspondant à l'étape
	nuser.send(`${etape.message}` + (etape.expected ? `\nRéponses attendues : \`${etape.expected.join("/")}\`` : ""));

	return true;
}



async function receptionMessageNewUser(msg)
{
	//on récupère l'étape de l'utilisateur
	let s = newUsers[msg.author.id].step;

	//on vire les accents et les blancs
	let message = msg.content
					.trim()
					.toLowerCase()
					.replace('é', 'e')
					.replace('è', 'e')
					.replace('ê', 'e')
					.replace('à', 'a');

	//si ce qu'a envoyé l'utilisateur n'est pas conforme à la réponse attendue
	if(etapes[s].expected && !etapes[s].expected.includes(message))
		return msg.author.send("Désolé, je ne comprends pas ça...");
	if(etapes[s].min_char && etapes[s].max_char && (message.length > etapes[s].max_char || message.length < etapes[s].min_char))
		return msg.author.send(`Désolé, ta réponse doit faire entre ${etapes[s].min_char} et ${etapes[s].max_char} caractères...`);

	//on stocke les bonnes variables dans le profil de l'élève
	if(etapes[s].variable == 'semestre')
		newUsers[msg.author.id].semestre = message;
	if(etapes[s].variable == 'groupe')
		newUsers[msg.author.id].groupe = message;
	if(etapes[s].variable == 'name')
		newUsers[msg.author.id].name = message;
	
	//on vérifie s'il y a une ou plusieurs suites à l'inscription
	if(typeof etapes[s].next == 'object')
		newUsers[msg.author.id].step = etapes[s].next[message];
	else
		newUsers[msg.author.id].step = etapes[s].next;
	

	//si la suite == end, cela signifie qu'on a terminé l'inscription
	if(newUsers[msg.author.id].step == 'end')
		await validerInscription(msg.author,newUsers[msg.author.id], msg.client);
	
	//on lui envoie le message pour la suite/fin
	await envoiMessageInscription(msg.author);
		
	//on supprime l'utilisateur de la liste s'il a terminé l'inscription
	if(newUsers[msg.author.id].step == 'end')
		delete newUsers[msg.author.id];

}


//on valide l'inscription
async function validerInscription(user, insc, client)
{
	//on récupère les roles correspondant a chaque groupe et a chaque année
	const roles = require("../config/roles_annee.json");

	//on récupère l'id des roles correspondant a l'année et au groupe de l'utilisateur (et a la spé pour les S4)
	let role_annee_ID = null;
	let role_groupe_ID = null;
	let role_spe_ID = null;

	//si année 1	
	if(insc.semestre == '1' || insc.semestre == '2')
	{
		role_annee_ID = roles.A1.A1;

		if(insc.groupe != "autre")
			role_groupe_ID = roles.A1[insc.groupe];
	} //si année 2
	else if(insc.semestre == '3' || insc.semestre == '4')
	{
		role_annee_ID = roles.A2.A2;

		//au 3e semestre
		if(insc.semestre == '3' && insc.groupe != "autre")
			role_groupe_ID = roles.A2[insc.groupe];

		//au 4e semestre, on différencie os et at
		if(insc.semestre == '4' && insc.groupe != "autre")
		{	
			if(insc.groupe.includes("os"))
			{
				role_spe_ID = roles.A2.OS.OS;
				role_groupe_ID = roles.A2.OS[insc.groupe];
			}
			else if(insc.groupe.includes("at"))
			{
				role_spe_ID = roles.A2.AT.AT;
				role_groupe_ID = roles.A2.AT[insc.groupe];
			}
		}
	} //si LP
	else if(insc.semestre == '5' || insc.semestre == '6')
	{
		role_annee_ID = roles.LP.A3;

		if(insc.groupe != "autre")
			role_groupe_ID = roles.LP[insc.groupe];
	}
	else if(insc.semestre == 'ancien') //si ancien 
		role_annee_ID = roles.Ancien;
	else
		role_annee_ID = roles.Autre;

	
	//on récupère les roles
	let role_annee = null;
	let role_groupe = null;
	let role_spe = null;

	if(role_annee_ID != null)
		role_annee = await client.guilds.cache.get(serverID).roles.fetch(role_annee_ID);
	if(role_groupe_ID != null)
		role_groupe = await client.guilds.cache.get(serverID).roles.fetch(role_groupe_ID);
	if(role_spe_ID != null)
		role_spe = await client.guilds.cache.get(serverID).roles.fetch(role_spe_ID);

	//puis on lui attribue les bons roles
	if(role_annee_ID != null)
		client.guilds.cache.get(serverID).members.cache.get(user.id).roles.add(role_annee);
	if(role_groupe_ID != null)
		client.guilds.cache.get(serverID).members.cache.get(user.id).roles.add(role_groupe);
	if(role_spe_ID != null)
		client.guilds.cache.get(serverID).members.cache.get(user.id).roles.add(role_spe_ID);

	//On met bien une majuscule a chaque partie du prénom + nom
	let partieNom = insc.name.split(" ");

	for(let i = 0; i < partieNom.length; i++)
	{
		partieNom[i] = partieNom[i][0].toUpperCase() + partieNom[i].substring(1);
	}

	insc.name = partieNom.join(" ");

	//puis on rename l'utilisateur
	client.guilds.cache.get(serverID).members.cache.get(user.id).setNickname(insc.name);
}




async function resetGroups(client)
{
	console.log("Nouveau semestre");

	//on récupère tout les utilisateurs du serveur
	let users = await client.guilds.cache.get(serverID).members.fetch();

	//on récupère les roles pour les années et les groupes
	const roles = require("../config/roles_annee.json");
	const rolesANePasChanger = [roles.Professeur, roles.Ancien, roles.Autre]; //les anciens, les profs et les Autres ne vont pas changer d'un semestre a l'autre

	
	users.forEach(async user => {
		
		//si l'utilisateur est un bot, on laisse tomber
		if(user.user.bot)
			return;

		//on laisse tomber si l'utilisateur est un prof ou un ancien, il ne change pas d'année 
		for(let [id, role] of user.roles.cache)
		{
			if(rolesANePasChanger.includes(id))
			{
				return;
			}
		}
		
		//sinon pour chaque role, on vérifie si on doit enlever ce role
		for(let [id, role] of user.roles.cache)
		{
			//ouais c'est sale mais ca marche et c'est simple a mettre en place ¯\_(ツ)_/¯
			const rolesString = JSON.stringify(roles);

			if(rolesString.includes(role.id))
				user.roles.remove(role);	
		}

		//on fait comme si c'etait un nouvel utilisateur et c'est reparti mon kiki
		newUsers[user.id] = {
			step : 'eleve_annee',
			semestre : null,
			groupe : null,
			name : null		
		};

		await user.send("Salut ! On est reparti pour un nouveau semestre ! :smile: ");

		return envoiMessageInscription(user)

	});
}
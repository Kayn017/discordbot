const Discord = require("discord.js");

exports.name = "nickname";

exports.commands = [
    "/nickname {surnom} : ajoute à l'utilisateur le surnom demandé",
    "/removeNickname : retire le surnom de l'utilisateur"
];

exports.onMessage = async msg => {

    //taille maximale du pseudo discord ("nickname / pseudo")
    const NICKNAME_MAX_SIZE = 32;

    let para = msg.content.trim().split(' ');

    if (para[0] != '/nickname' && para[0] != '/removeNickname')
		return false;
		
	//on récupère l'auteur et son vrai nom
	const author = msg.guild.members.cache.get(msg.author.id);
	const oldNickname = author.displayName;
	const realName = oldNickname.split("/").pop().trim();
		
    if (para.length >= 2 && para[0] == '/nickname')
    { 

		//on calcule la taille max du nickname
		const max_char_nickname = NICKNAME_MAX_SIZE - realName.length - 3;

		//on vire la commande de la liste des arg
		para.shift();

		//on choppe son nouveau nickname
		const newNickname = para.join(' ');

		//si le nouveau nickname est trop long => on avertit l'utilisateur
		if(newNickname.length > max_char_nickname)
		{
			const embed = new Discord.MessageEmbed()
							.setColor(0xFF0000)
							.setDescription(`Ce pseudo est trop long :/ Il doit faire ${max_char_nickname} caractères de long au maximum`);
							
			msg.channel.send(embed);
			return true;
		}
		else //sinon on rename
		{
			author.setNickname(newNickname + " / " + realName);

			const embed = new Discord.MessageEmbed()
							.setColor(0xFF0000)
							.setDescription("Ton surnom a bien été changé :)");

			msg.channel.send(embed);
			return true;
		}


        
	}
	else if(para[0] == '/removeNickname')
	{
		author.setNickname(realName);

		const embed = new Discord.MessageEmbed()
						.setColor(0xFF0000)
						.setDescription("Ton surnom a bien été enlevé :)");

		msg.channel.send(embed);
		return true;
	}

}